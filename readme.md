Universidad Católica "Nuestra Señora de la Asunción"  
Facultad de Ciencia y Tecnología  
Departamento de Analisis de Sistemas  
Ingeniería del Software 4

# Ejercicios de progrmación #2

## Pasos para correr este repositorio

Una vez clonado el repo:

1. Instalar [Docker](https://www.docker.com/) para la plataforma local.
2. Una vez instalado, navegar al directorio del proyecto y correr el comando `docker build . -t is4-ej2-php`
3. Una vez creada la imagen, correr el comando `docker run -p 3000:80 -v $(pwd)/src:/var/www/html is4-ej2-php`
