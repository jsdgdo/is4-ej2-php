<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>JD - Ejercicio 4</title>
</head>
<body>
  <?php  
    $v = 24.5;  
    echo gettype($v).'<br />';
    $v = 'HOLA';
    echo gettype($v).'<br />';
    settype($v, 'int');
    echo var_dump($v);
  ?>

</body>
</html>