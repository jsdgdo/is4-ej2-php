<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>JD - Ejercicio 5</title>
  <style>
    table {border-collapse: collapse}
    td {border: 1px solid black; padding: 4px;}
    tbody tr:nth-child(even) {
      background-color: gray;
    }
  </style>
</head>
<body>
  <table>
  <thead>
    <tr>
      <td colspan="2">Tabla del 9</td>
    </tr>
    <tr>
      <td>Operación</td>
      <td>Resultado</td>
    </tr>
  </thead>
  <tbody>
  <?php for ($i = 1; $i < 11; $i++) {?>
    <tr>
      <td>
        9 x <?php echo $i;?>
      </td>
      <td>
        <?php echo 9*$i; ?>
      </td>
    </tr>
  <?php } ?>
  </tbody>
  </table>
</body>
</html>