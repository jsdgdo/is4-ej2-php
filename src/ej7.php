<?php 
  $parcial1 = rand(0, 30);
  $parcial2 = rand(0, 20);
  $final1 = rand(0, 50);

  $acum = $parcial1 + $parcial2 + $final1; 

  switch ($acum) {
    case ($acum > 59 && $acum < 70):
      echo 'El alumno tuvo nota 2';
      break;
    case ($acum > 69 && $acum < 80):
      echo 'El alumno tuvo nota 3';
      break;
    case ($acum > 79 && $acum < 90):
      echo 'El alumno tuvo nota 4';
      break;
    case ($acum > 89):
      echo 'El alumno tuvo nota 5';
      break;
    default:
      echo 'El alumno tuvo nota 1';
      break;
  }
?>